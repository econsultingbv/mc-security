# Preparation
Please complete the following steps before the Masterclass Security.

## Necessary
The following software needs to be installed and configured before the Masterclass.  

- Install Burp Suite (Community Edition 2023.1.2)

## Setup Burp Suite
In order to setup Burp Suite, please complete the following steps:

- Clone the GIT repository https://gitlab.com/econsultingbv/mc-security.git
- Open the following URL in the browser: https://mcs-petclinic-0.econsulting.nl
- Enter the following username and password:
    - username: owner
    - password: password
- Press Login, you should see the PetClinic application

Now start Burp Suite and select the following options:

- Select Temporary project and click Next
- Select Load from configuration file, select <mc-security dir>/setup/burpsuite.json and click Start Burp
- Select Proxy tab and click Open Browser
- Open the following URL in the browser: https://mcs-petclinic-0.econsulting.nl
- The browser will keep spinning and Burp Suite should have intercepted the request
- Click Forward in Burp Suite and the browser should show the login screen

## Background material
The following articles are a nice warming up for the Masterclass: 

- https://medium.com/@alex.birsan/dependency-confusion-4a5d60fec610
- https://medium.com/@jsoverson/exploiting-developer-infrastructure-is-insanely-easy-9849937e81d4
- https://citizenlab.ca/2022/01/cross-country-exposure-analysis-my2022-olympics-app

Feel free to share other articles, blogs and video's about these subjects with the other members in your talent class.

